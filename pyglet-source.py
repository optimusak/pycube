import pyglet
import pyglet.math as glMath
from pyglet.gl import GL_TRIANGLES,GL_LINES
from pyglet.math import Vec3,Mat4,Vec4
from pyglet.graphics.shader import Shader,ShaderProgram
PI = 3.1415926535
vertex_source = """
#version 330 core
layout(location=0) in vec3 vertices;
layout(location=1) in vec4 color;

out vec4 fragColor;
uniform mat4 tf;
void main(){
    gl_Position = tf*vec4(vertices,1.0);
    fragColor = color;
}

"""

fragment_source = """
#version 330 core
out vec4 color;
in vec4 fragColor;
void main(){
    color = fragColor;
}

"""
vertex_shader = Shader(vertex_source,'vertex')
fragment_shader = Shader(fragment_source,'fragment')
shader = ShaderProgram(vertex_shader,fragment_shader)
window = pyglet.window.Window(width=800,height=400,caption="Pyglet screen")
batch = pyglet.graphics.Batch()
circle = pyglet.shapes.Circle(0,0,100,batch=batch)
depth = 300
near = 400
z0 = -(depth/2)-near

v_list = (
            -50,-50, -50,
             50,-50, -50,
             50, 50, -50,
            -50, 50, -50,
            -50,-50,  50,
             50,-50,  50,
             50, 50,  50,
            -50, 50,  50)
indices = [
    0,1,1,2,2,3,3,0,
    4,5,5,6,6,7,7,4,
    0,4,1,5,2,6,3,7
]
window.x = 0
# v = glMath.Mat4.from_translation([0,0,-1])
# orthogonalMatrix = glMath.Mat4.orthogonal_projection(0,window.width,0,window.height,-100,100)
rotation = glMath.Mat4.from_rotation(0.00001,glMath.Vec3(0,1,0))
oM = glMath.Mat4([2.0/window.width, 0.0, 0.0, 0.0,
                  0.0, 2/window.height, 0.0, 0.0,
                  0.0, 0.0, 2/depth, 0.0,
                  0.0, 0.0, 0.0,1.0])

prespective = glMath.Mat4([
                    near,   0,0,0,
                    0,   near,0,0,
                    0,      0,1,1.,
                    0,      0,0,-z0
])

oM = oM@prespective
s = Vec4(100,100,0,1)

window.t = 0
# print(oM@s)
# print(translate)
shader['tf'] = rotation@oM
window.delta = 0
theta = PI/120
# print(translate@Vec4(0,0,100,1))
v_list = shader.vertex_list_indexed(8,GL_LINES,
                                        indices=indices,
                                        vertices=('f',v_list),
                                        color=('Bn',(255,255,0,255,
                                                    0,255,0,255,
                                                    255,0,0,255,
                                                    0,0,255,255,
                                                    255,0,0,255,
                                                    0,0,255,255,
                                                    255,255,255,255,
                                                    0,255,0,255)),
                                        batch=batch)
@window.event
def on_draw():
    # global rotation
    window.clear()
    batch.draw()
    window.delta += theta
    translate = Mat4.from_translation(Vec3(0,0,window.x))
    rotation = glMath.Mat4.from_rotation(window.delta,glMath.Vec3(0,1,0))
    rotation2 = glMath.Mat4.from_rotation(window.delta,glMath.Vec3(1,0,0))
    shader['tf'] = oM@translate@rotation@rotation2
    window.x += window.t
    print(window.x)
@window.event
def on_key_press(key,modifer):
    if(key == pyglet.window.key.A):
        window.t = 10
    if(key == pyglet.window.key.D):
        window.t = -10

@window.event
def on_key_release(key,modifiers):
    window.t = 0

pyglet.app.run()