import numpy as np


import numpy as np
from numpy import sin,cos,pi
PI = pi
angle = PI/120

class Transformers3D:
    projection = np.array([
        [1, 0,  0,  0],
        [0, 1,  0,  0],
        [0, 0,  0,  1]
    ])


    rotateX = np.array([
        [1,              0,            0,    0],
        [0,     cos(angle),  -sin(angle),    0],
        [0,     sin(angle),   cos(angle),    0],
        [0,              0,            0,    1]
    ])

    rotateY = np.array([
        [ cos(angle),    0,   sin(angle),    0],
        [          0,    1,            0,    0],
        [-sin(angle),    0,   cos(angle),    0],
        [          0,    0,            0,    1]
    ])

    rotateZ = np.array([
        [cos(angle),   -sin(angle),   0,    0],
        [sin(angle),    cos(angle),   0,    0],
        [         0,             0,   1,    0],
        [         0,             0,   0,    1]
    ])

    scale = np.array([
        [1,0,0,0],
        [0,1,0,0],
        [0,0,1,0],
        [0,0,0,1]
    ])


class Transformers2D:

    def  __init__(self):
        self.rotate = np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ],dtype='float64')

        self.scale = np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ],dtype='float64')

    def updateRotation(self,angle:float):
        self.rotate[0,0] =  np.cos(angle)
        self.rotate[0,1] = -np.sin(angle)
        self.rotate[1,0] =  np.sin(angle)
        self.rotate[1,1] =  np.cos(angle)
    
    def updateScale(self,scalex,scaley):
        self.scale[0,0] = scalex
        self.scale[1,1] = scaley
    
