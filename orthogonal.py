#TODO 
#TODO transformations in 2D
#TODO transformations in 3D

import pygame
import numpy as np
from transformers import Transformers2D,Transformers3D

transformer = Transformers2D()
transformer.updateRotation(np.pi/60)
transformer.updateScale(1.01,1.01)
final = transformer.rotate@transformer.scale
translate = np.array([
    [1, 0, 0,  0],
    [0, 1, 0,  0],
    [0, 0, 1,  3],
    [0, 0, 0,  1]
],dtype=np.float32)

vel_vector = np.zeros(4)
class Vertices:
    #homogeneous coordinates
    vertices = np.array([
        [-0.5, -0.5, -0.5, 1],
        [ 0.5, -0.5, -0.5, 1],
        [ 0.5,  0.5, -0.5, 1],
        [-0.5,  0.5, -0.5, 1],
        [-0.5, -0.5,  0.5, 1],
        [ 0.5, -0.5,  0.5, 1],
        [ 0.5,  0.5,  0.5, 1],
        [-0.5,  0.5,  0.5, 1]
    ]).transpose()



edges = [(0,1),(1,2),(2,3),(3,0),(0,4),(1,5),(2,6),(3,7),(4,5),(5,6),(6,7),(7,4)]

# Vertices.vertices = transformer.rotate@Vertices.vertices
sens = 0.04
class App:

    def __init__(self,width,height,caption):
        self.fps = 60
        self.width = width
        self.height = height
        self.exit_window = False
        self.bg_color = (12,23,16)
        self.point_color = (255,232,100)
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((self.width,self.height))
        pygame.display.set_caption(caption)
        self.theta = np.pi/20
        self.v = np.pi/20
        self.near = 1.5
        self.far = 10
        self.right = 1
        self.top = 1
        self.ortho = self.work()

    def loop(self):
        
        while not self.exit_window:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.exit_window = True
            
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_a:
                        vel_vector[0] = -sens
                    if event.key == pygame.K_d:
                        vel_vector[0] =  sens
                    if event.key == pygame.K_w:
                        vel_vector[1] =  sens
                    if event.key == pygame.K_s:
                        vel_vector[1] = -sens
                    if event.key == pygame.K_z:
                        vel_vector[2] =  sens
                    if event.key == pygame.K_x:
                        vel_vector[2] = -sens

                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_a:
                        vel_vector[0] = 0.0
                    if event.key == pygame.K_d:
                        vel_vector[0] = 0.0
                    if event.key == pygame.K_w:
                        vel_vector[1] = 0.0
                    if event.key == pygame.K_s:
                        vel_vector[1] = 0.0
                    if event.key == pygame.K_z:
                        vel_vector[2] = 0.0
                    if event.key == pygame.K_x:
                        vel_vector[2] = 0.0
                

            
            pygame.display.update()
            self.screen.fill(self.bg_color)
            self.render()
            self.clock.tick(self.fps)
            translate[:,-1] += vel_vector

    def render(self):
        # Vertices.vertices = Vertices.vertices/Vertices.vertices[-1]
        Vertices.vertices = Transformers3D.rotateY@(Vertices.vertices)
        v = translate@Vertices.vertices
        points = self.ortho@v
        points = points/(points[-1])
        # print(points)
        # exit()
        points = (self.translate@points).transpose()
        # exit()
        for point in points:
            self.drawPoint(point)

        for x1,x2 in edges:
            pygame.draw.line(self.screen,self.point_color,points[x1],points[x2])
        #now points is 2x8 matrix
        # print(self.theta)
        

    def drawPoint(self,point,stroke=5):
        pygame.draw.circle(self.screen,self.point_color,point,stroke)
    
    def work(self):
        n1 = 1/self.right
        n2 = 1/self.top
        n3 = 1/(self.far-self.near)
        n4 = (self.near)*n3
        ortho = np.array([
            [n1,  0,  0,  0],
            [ 0, n2,  0,  0],
            [ 0,  0,  n3, n4],
            [ 0,  0,  0,  1]
        ])

        prespective = np.array([
            [self.near, 0, 0, 0],
            [0, self.near, 0, 0],
            [0, 0,   self.near+self.far, -self.far],
            [0, 0,   1, 0]
        ])

        self.translate = np.array([
            [self.width/2,  0, 0,  self.width/2],
            [0, -self.height/2, 0, self.height/2]
        ])
        mat = ortho@prespective
        return mat

if __name__ == '__main__':
    app = App(500,500,'Cube')
    app.loop()
    # app.loop()